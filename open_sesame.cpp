#define WIN32_LEAN_AND_MEAN 
#include <windows.h>

#include <winsock2.h>
#include <iphlpapi.h>
#include <natupnp.h>
#include <conio.h>

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>

typedef uint16_t u16;
typedef uint32_t u32;
typedef int32_t i32;

#define ARRAYLEN(a) (sizeof(a) / sizeof(a[0]))
#ifdef _DEBUG
#define OOPS(b) if(b) { (*((char*) 0)) = 0; }
#else
#define OOPS(b)
#endif

struct PortForward
{
    BSTR ipAddress;
    IUPnPNAT* natService;
    IStaticPortMappingCollection* portMappings;
};
PortForward pf;

//NOTE : "Constants" in COM land
BSTR PROTOCOL_UDP;
BSTR PROTOCOL_TCP;
BSTR DESCRIPTION;

BSTR GetFirstAdapterIP()
{
    ULONG flags = (
        GAA_FLAG_SKIP_ANYCAST | 
        GAA_FLAG_SKIP_MULTICAST | 
        GAA_FLAG_SKIP_DNS_SERVER | 
        GAA_FLAG_SKIP_FRIENDLY_NAME |
        GAA_FLAG_SKIP_DNS_INFO
    );

    BSTR result = NULL;
    ULONG size = 0;
    ULONG status = GetAdaptersAddresses(
        AF_INET,
        flags,
        NULL,
        NULL,
        &size
    );
    PIP_ADAPTER_ADDRESSES adapters = (PIP_ADAPTER_ADDRESSES) _alloca(size);
    status = GetAdaptersAddresses(
        AF_INET,
        flags,
        NULL,
        adapters,
        &size
    );

    PIP_ADAPTER_ADDRESSES currentAdapter = adapters;
    do
    {
        //NOTE : might want to select other IfTypes too
        if (currentAdapter->OperStatus == IfOperStatusUp &&
            (currentAdapter->IfType == IF_TYPE_ETHERNET_CSMACD || currentAdapter->IfType == IF_TYPE_IEEE80211))
        {
            sockaddr_in* addr = (sockaddr_in*)currentAdapter->FirstUnicastAddress->Address.lpSockaddr;
            wchar_t ipString[32];
            IN_ADDR a = addr->sin_addr;
            _snwprintf_s(ipString, ARRAYLEN(ipString), _TRUNCATE, L"%d.%d.%d.%d",
                a.S_un.S_un_b.s_b1,
                a.S_un.S_un_b.s_b2,
                a.S_un.S_un_b.s_b3,
                a.S_un.S_un_b.s_b4
            );
            result = SysAllocString(ipString);
            break;
        }
    } while (currentAdapter = currentAdapter->Next);

    return result;
}

PortForward PortForwardInit()
{
    PortForward portForward = {};
    portForward.ipAddress = GetFirstAdapterIP();
    if (portForward.ipAddress)
    {
        printf("[NETWORK] adapter with ip found : %ls\n", portForward.ipAddress);

        if (SUCCEEDED(CoCreateInstance(
            __uuidof(UPnPNAT),
            NULL,
            CLSCTX_INPROC_SERVER,
            __uuidof(IUPnPNAT), (LPVOID*) &portForward.natService)))
        {
            if (SUCCEEDED(portForward.natService->get_StaticPortMappingCollection(&portForward.portMappings)))
            {
                if (portForward.portMappings == NULL)
                {
                    printf("[NAT] Error : UPnP is not working\n");
                }
            }
            else
            {
                printf("[COM] Error : cannot get nat portmap\n");
            }
        }
        else
        {
            printf("[COM] Error : cannot locate nat service\n");
        }
    }
    else
    {
        printf("[NETWORK] Error : adapter not found\n");
    }

    if (portForward.portMappings == NULL)
    {
        if (portForward.natService)
        {
            portForward.natService->Release();
            portForward.natService = NULL;
        }

        if (portForward.ipAddress)
        {
            SysFreeString(portForward.ipAddress);
            portForward.ipAddress = NULL;
        }
    }

    return portForward;
}

void PortForwardAddEntry(PortForward* pf, u16 port, BSTR protocol, BSTR decriptor)
{
    IStaticPortMapping* portMapping = NULL;
    if (SUCCEEDED(pf->portMappings->Add(port, protocol, port, pf->ipAddress, VARIANT_TRUE, DESCRIPTION, &portMapping)))
    {
        printf("[NAT] port added [%ls] [%ls:%d]\n", protocol, pf->ipAddress, port);
        portMapping->Release();
    }
    else
    {
        printf("[NAT] Error : cannot add port [%ls] [%ls:%d]\n", protocol, pf->ipAddress, port);
    }
}

void PortForwardRemoveEntry(PortForward* pf, u16 port, BSTR protocol)
{
    if (SUCCEEDED(pf->portMappings->Remove(port, protocol)))
    {
        printf("[NAT] port removed [%ls] [%ls:%d]\n", protocol, pf->ipAddress, port);
    }
    else
    {
        printf("[NAT] Error : cannot remove port [%ls] [%ls:%d]\n", protocol, pf->ipAddress, port);
    }
}

void PortForwardFinish(PortForward* pf)
{
    if (pf->portMappings) pf->portMappings->Release();
    if (pf->natService) pf->natService->Release();
    if (pf->ipAddress) SysFreeString(pf->ipAddress);

    (*pf) = {};
}

void InitApp()
{
    CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

    PROTOCOL_UDP = SysAllocString(L"UDP");
    PROTOCOL_TCP = SysAllocString(L"TCP");
    DESCRIPTION = SysAllocString(L"OpenSesame");

    pf = PortForwardInit();
}

void FinishApp()
{
    PortForwardFinish(&pf);

    SysFreeString(PROTOCOL_UDP);
    SysFreeString(PROTOCOL_TCP);
    SysFreeString(DESCRIPTION);

    CoUninitialize();
}

int main(int argc, char** args)
{
    u32 portCount = (argc - 1);
    if (portCount != 0)
    {
        i32* ports = (i32*) _alloca(portCount * sizeof(i32));
        for (u32 i = 0; i < portCount; ++i)
        {
            char* arg = args[i + 1];
            i32 port = -1;
            sscanf_s(arg, "%I32d", &port);
            port = (port > 65535) ? (-1) : (port);
            port = (port < 0) ? (-1) : (port);

            if (port >= 0)
            {
                printf("[APP] argument %d parsed as port : %I32d\n", i, port);
            }
            else
            {
                printf("[APP] Error : argument %d cannot be parsed as port : %s\n", i, arg);
            }

            ports[i] = port;
        }

        InitApp();

        if (pf.portMappings)
        {
            for (u32 i = 0; i < portCount; ++i)
            {
                if (ports[i] >= 0)
                {
                    PortForwardAddEntry(&pf, ports[i], PROTOCOL_TCP, DESCRIPTION);
                    PortForwardAddEntry(&pf, ports[i], PROTOCOL_UDP, DESCRIPTION);
                }
            }

            printf("[APP] Port mappings added. Keep this program open while you are using the ports\n");
        }

        if (pf.portMappings)
        {
            printf("[APP] Press enter to remove port mappings...\n");
            getc(stdin);
            for (u32 i = 0; i < portCount; ++i)
            {
                if (ports[i] >= 0)
                {
                    PortForwardRemoveEntry(&pf, ports[i], PROTOCOL_TCP);
                    PortForwardRemoveEntry(&pf, ports[i], PROTOCOL_UDP);
                }
            }
        }
        FinishApp();
    }
    else
    {
        printf("[APP] Error : 0 ports specified. Please list ports as arguments separated by space.\n");
    }

    printf("[APP] Press enter to exit.");
    getc(stdin);

    return 0;
}